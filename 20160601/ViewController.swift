    //
//  ViewController.swift
//  20160601
//
//  Created by nkfust16_2 on 2016/6/22.
//  Copyright © 2016年 nkfust16_2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var Show: UILabel!
    var 正在輸入 = false
    @IBAction func Button1to9(sender: UIButton) {
        let msg = sender.currentTitle!
        if Show.text == "0" {
            Show.text = " "
        }
        if Show.text == "0.0"{
            Show.text = " "
        }
        Show.text = Show.text! + msg
        print("Touch \(msg)")
        
    }
    @IBAction func Button0(sender: UIButton) {
        let msg = sender.currentTitle!
        
        if Show.text == "0" {
            Show.text = ""
        }
        else if Show.text == "0.0"{
            Show.text = ""
        }
        Show.text = Show.text! + msg
        print("Touch \(msg)")
        
        
        
    }
    @IBAction func ButtonC(sender: UIButton) {
        if 正在輸入 {
            model.baibai(ShowValue)
            正在輸入 = false
        }
        
        if let math = sender.currentTitle{
            model.gobaibai(math)
        }
        ShowValue = model.result
        
        
        
        
    }
    
    var ShowValue: Double {
        get{
            return Double(Show.text!)!
        }
        set {
            Show.text = String(newValue)
        }
    }
    private var model = testmodel()
    
    @IBAction func 加減乘除(sender: UIButton) {
        if 正在輸入 {
            model.baibai(ShowValue)
            正在輸入 = false
        }
        
        if let mathsymbol = sender.currentTitle {
            model.performOperation(mathsymbol)
        }
        ShowValue = model.result
        
        
    }
}